package com.example.soapparse;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathFactory;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.nio.file.Files;

@Slf4j
public class SoapParseTest {

    @Test
    public void given_when_then() throws Exception {
        String path = "soapRequest.xml";
        String request = getFileContent(path);

        InputStream is = new ByteArrayInputStream(request.getBytes());
        SOAPMessage soapMessage = MessageFactory.newInstance().createMessage(null, is);

        SOAPPart part = soapMessage.getSOAPPart();
        part.detachNode();
        SOAPEnvelope env = part.getEnvelope();
        env.detachNode();
        SOAPBody body = env.getBody();

        body.detachNode();

        Document bodyDoc = body.extractContentAsDocument();

        NodeList in0 = bodyDoc.getElementsByTagName("in0");
        Node in0Node = in0.item(0);
        log.info("in0: [{}]", in0Node.getTextContent());
        NodeList in1 = bodyDoc.getElementsByTagName("in1");
        Node in1Node = in1.item(0);
        log.info("in1: [{}]", in1Node.getTextContent());

        InputStream bais = new ByteArrayInputStream(in1Node.getTextContent().getBytes("UTF-8"));
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document doc = builder.parse(bais);

        XPathFactory xPathFactory = XPathFactory.newInstance();
        XPath xpath = xPathFactory.newXPath();
        String value = xpath.evaluate("//createBatch/createBatchOptions/ClientInfo/ClientName/text()", doc);
        log.info("ClientName: [{}]", value);
    }

    private String getFileContent(final String path) throws Exception {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource(path).getFile());
        return new String(Files.readAllBytes(file.toPath()));
    }

}
